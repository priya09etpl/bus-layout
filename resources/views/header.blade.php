<!DOCTYPE html>
<html lang="en">
<head>
<title>Layout</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
/* Style the body */
body {
  font-family: Arial;
  margin: 0;
}

/* Header/Logo Title */
.header {
  padding: 5px;
  text-align: center;
  background: #1abc9c;
  color: white;
  font-size: 30px;
}

/* Page Content */
.content {
    margin: auto;
    width: 60%;
    color: black;
    text-align: center;
    padding: 10px;
}

</style>
</head>
<body>

<div class="header">
  <h1>Bus Layout</h1>
  <p></p>
</div>