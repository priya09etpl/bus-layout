<?php namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class InfoModel extends Model
{
    public static function getInfo()
    {
        $client = new Client();

        $res = $client->request('POST', 'https://www.smsipl.com/Gasco/seat-map.php?key=3ec9864e9db5fb479a338d108503d4612fc65b80', [
            'form_params' => [
                'auth_key' => 2,
                'password' => 1234
            ]
        ]);

        if ($res->getStatusCode() == 200) {
            $response_data = $res->getBody()->getContents();
        }
        return $response_data;
    }
}