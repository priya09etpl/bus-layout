<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Model\InfoModel;

class LayoutController extends BaseController
{
    public function index()
    {
        $data = InfoModel::getInfo();
        $allInfo = json_decode($data);
        if($allInfo->status == 200)
        {
            $returnData = $allInfo;
        }
        else
        {
            $returnData = array();
        }
        
        return view('layout', ['displayData' => $returnData]);
    }

    public static function getSetRow($row)
    {
        $parts = explode(',', $row);
        $cells = "";
        foreach ($parts as  $value) {
            $cells .=  LayoutController::getSeat($value);
        }
        return $cells;
    }

    public static function getSeat($cell)
    {
        if ($cell != '0') {
            $nos = explode('-', $cell);
            return $ret = '<td width="60" style="border:1px solid black">'.$nos[3].'</td>';
        } else {
            return $ret = '<td width="60"> </td>';
        }
    }

}